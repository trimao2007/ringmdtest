package currentWeather.datamanager;

import object.Weather;

/**
 * Created by trimao on 1/29/16.
 */
public interface IOnUpdateWeatherListener {
    void onSuccess(Weather weather);
    void onFailed();
}
