package currentWeather.datamanager;

import android.content.Context;

import currentWeather.presenter.WeatherCityPresenterImpl;

/**
 * Created by trimao on 1/29/16.
 */
public interface WeatherCityDataManager {
    void getCurrentWeather(final WeatherCityPresenterImpl.OnUpdateWeather listener, String city, Context context);
}
