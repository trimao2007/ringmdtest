package currentWeather.datamanager;

import android.content.ContentValues;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import currentWeather.presenter.WeatherCityPresenterImpl;
import object.Weather;
import request.API;
import request.RingMDRequest;

/**
 * Created by trimao on 1/29/16.
 */
public class WeatherCityDataManagerImpl implements WeatherCityDataManager {

    @Override
    public void getCurrentWeather(final WeatherCityPresenterImpl.OnUpdateWeather listener, String city, Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);
        ContentValues params = new ContentValues();
        params.put("q", city);
        RingMDRequest request = new RingMDRequest(API.WEATHER.LOCAL_WEATHER, params, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                try {
                    String responseString = String.valueOf(response);
                    JSONObject jsonObject = new JSONObject(responseString);
                    Weather weather = new Weather();
                    weather.initialize(jsonObject);
                    listener.onSuccess(weather);
                }catch (JSONException ex) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);

    }
}
