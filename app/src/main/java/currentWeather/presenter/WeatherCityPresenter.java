package currentWeather.presenter;

import android.support.annotation.Nullable;

import currentWeather.view.WeatherCityViewImpl;

/**
 * Created by trimao on 1/28/16.
 */
public interface WeatherCityPresenter {
    void handleCreateView();
    void getWeatherData(@Nullable WeatherCityViewImpl.CompleteUpdatedWeather completeUpdatedWeather);
}
