package currentWeather.presenter;

import android.support.annotation.Nullable;

import currentWeather.datamanager.IOnUpdateWeatherListener;
import currentWeather.datamanager.WeatherCityDataManager;
import currentWeather.datamanager.WeatherCityDataManagerImpl;
import currentWeather.view.WeatherCityView;
import currentWeather.view.WeatherCityViewImpl;
import object.Weather;
import utils.DateTimeHelper;

/**
 * Created by trimao on 1/28/16.
 */
public class WeatherCityPresenterImpl implements WeatherCityPresenter {

    private WeatherCityView weatherCityView;
    private String city;

    public WeatherCityPresenterImpl(WeatherCityView viewModel, String city) {
        this.weatherCityView = viewModel;
        this.city = city;
    }

    @Override
    public void handleCreateView() {
        weatherCityView.initialControlInView();
        getWeatherData(null);
    }

    @Override
    public void getWeatherData(@Nullable WeatherCityViewImpl.CompleteUpdatedWeather completeUpdatedWeather) {
        WeatherCityDataManager weatherCityDataManager = new WeatherCityDataManagerImpl();
        weatherCityDataManager.getCurrentWeather(new OnUpdateWeather(), city, weatherCityView.getMainActivity());

        if (completeUpdatedWeather != null) {
            completeUpdatedWeather.onFinished(DateTimeHelper.getCurrentDateTime());
        }
    }

    public class OnUpdateWeather implements IOnUpdateWeatherListener {

        @Override
        public void onSuccess(Weather weather) {
            weatherCityView.fillDataIntoView(weather);
        }

        @Override
        public void onFailed() {
            weatherCityView.fillDataIntoView(null);
        }
    }

}
