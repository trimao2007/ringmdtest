package currentWeather.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.trimao.ringmdtest.R;

import currentWeather.presenter.WeatherCityPresenter;
import currentWeather.presenter.WeatherCityPresenterImpl;
import view.CityListAdapter;

/**
 * Created by trimao on 1/28/16.
 */
public class WeatherCityFragment extends Fragment {

    private String city;
    private WeatherCityPresenter presenter;

    public static WeatherCityFragment newInstance(Bundle bundle) {
        WeatherCityFragment weatherCityFragment = new WeatherCityFragment();
        weatherCityFragment.city = bundle.getString(CityListAdapter.CITY, "");
        return weatherCityFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.current_weather_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WeatherCityView viewModel = new WeatherCityViewImpl(getActivity(), view, city);
        presenter = new WeatherCityPresenterImpl(viewModel, city);
        presenter.handleCreateView();

    }
}