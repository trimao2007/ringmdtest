package currentWeather.view;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import object.Weather;

/**
 * Created by trimao on 1/28/16.
 */
public interface WeatherCityView {
    // init all control in the fragment
    void initialControlInView();

    FragmentActivity getMainActivity();

    //fill data in to controls after getting the response from the server
    void fillDataIntoView(@Nullable Weather weather);
}
