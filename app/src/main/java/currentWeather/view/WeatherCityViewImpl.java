package currentWeather.view;

import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.trimao.ringmdtest.R;
import com.squareup.picasso.Picasso;

import currentWeather.presenter.WeatherCityPresenter;
import currentWeather.presenter.WeatherCityPresenterImpl;
import object.Weather;
import utils.DateTimeHelper;

/**
 * Created by trimao on 1/28/16.
 */
public class WeatherCityViewImpl implements WeatherCityView, View.OnClickListener {

    private FragmentActivity mainActivity;
    private View mainView;
    private String city;
    private TextView tempCTextView;
    private TextView tempFTextView;
    private TextView humidityTextView;
    private TextView pressureTextView;
    private TextView weatherDescriptionTextView;
    private TextView visibilityTextView;
    private TextView updatedAtTextView;
    private TextView cityTextView;

    private ImageView weatherImageView;
    private WeatherCityPresenter presenter;

    private ProgressBar loadingBar;
    private View weatherLayout;

    public WeatherCityViewImpl(FragmentActivity activity, View view, String city) {
        this.mainActivity = activity;
        this.mainView = view;
        this.city = city;
        this.presenter = new WeatherCityPresenterImpl(this, this.city);
    }

    @Override
    public void initialControlInView() {
        cityTextView = (TextView) mainView.findViewById(R.id.txtCity);
        tempCTextView = (TextView) mainView.findViewById(R.id.txtTempC);
        tempFTextView = (TextView) mainView.findViewById(R.id.txtTempF);
        humidityTextView = (TextView) mainView.findViewById(R.id.txtHumidity);
        pressureTextView = (TextView) mainView.findViewById(R.id.txtPressure);
        weatherImageView = (ImageView) mainView.findViewById(R.id.imageWeather);
        weatherDescriptionTextView = (TextView) mainView.findViewById(R.id.txtWeatherDesc);
        visibilityTextView = (TextView) mainView.findViewById(R.id.txtVisibility);
        updatedAtTextView = (TextView) mainView.findViewById(R.id.txtUpdated);
        loadingBar = (ProgressBar) mainView.findViewById(R.id.loading);
        weatherLayout = mainView.findViewById(R.id.weather_layout);
        FloatingActionButton updateButton = (FloatingActionButton) mainActivity.findViewById(R.id.updateFloatingButton);
        updateButton.setOnClickListener(this);
    }

    @Override
    public FragmentActivity getMainActivity() {
        return mainActivity;
    }

    @Override
    public void fillDataIntoView(@Nullable Weather weather) {
        if (weather == null) return;
        weatherLayout.setVisibility(View.VISIBLE);
        loadingBar.setVisibility(View.GONE);
        cityTextView.setText(String.format(mainActivity.getString(R.string.current_weather_at_time), city,
                DateTimeHelper.getHoursFromDateString(weather.getObservationTime())));
        tempCTextView.setText(String.valueOf(weather.getTemp_C()) + (char) 0x00B0 + "C");
        tempFTextView.setText(String.valueOf(weather.getTemp_F()) + (char) 0x00B0 + "F");
        weatherDescriptionTextView.setText(weather.getWeatherDesc());
        visibilityTextView.setText(String.format(mainActivity.getString(R.string.visibility), String.valueOf(weather.getVisibility())));
        humidityTextView.setText(String.format(mainActivity.getString(R.string.humidity), String.valueOf(weather.getHumidity())).concat("%"));
        pressureTextView.setText(String.format(mainActivity.getString(R.string.pressure), String.valueOf(weather.getPressure())));
        updatedAtTextView.setText(String.format(mainActivity.getApplicationContext().getString(R.string.updated_at), DateTimeHelper.getCurrentDateTime()));
        Picasso.with(mainActivity.getApplicationContext()).load(weather.getWeatherIconUrl()).into(weatherImageView);
    }

    @Override
    public void onClick(View view) {
        presenter.getWeatherData(new CompleteUpdatedWeather() {
            @Override
            public void onFinished(String latestUpdateTime) {
                updatedAtTextView.setText(String.format(mainActivity.getString(R.string.updated_at), latestUpdateTime));
            }
        });
    }

    public interface CompleteUpdatedWeather {
        void onFinished(String latestUpdateTime);
    }
}
