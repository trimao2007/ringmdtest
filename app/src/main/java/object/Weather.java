package object;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by trimao on 1/28/16.
 */
public class Weather {

    public static final String JSON_TAG_DATA = "data";
    public static final String JSON_TAG_CURRENT_CONDITION = "current_condition";
    public static final String JSON_TAG_HUMIDITY = "humidity";
    public static final String JSON_TAG_PRESSURE = "pressure";
    public static final String JSON_TAG_TEMP_C = "temp_C";
    public static final String JSON_TAG_TEMP_F = "temp_F";
    public static final String JSON_TAG_VISIBILITY = "visibility";
    public static final String JSON_TAG_LOCAL_OBS_DATE_TIME = "localObsDateTime";
    public static final String JSON_TAG_WEATHER_DESC = "weatherDesc";
    public static final String JSON_TAG_VALUE = "value";
    public static final String JSON_TAG_WEATHER_ICON_URL = "weatherIconUrl";
    private int humidity;
    private int pressure;
    private int temp_C;
    private int temp_F;
    private String weatherDesc;
    private String weatherIconUrl;
    private double visibility;
    private String observationTime;

    public Weather() {
    }

    public void initialize(JSONObject jsonObject) throws JSONException{
        JSONObject data = jsonObject.getJSONObject(JSON_TAG_DATA);
        JSONArray currentCondition = data.getJSONArray(JSON_TAG_CURRENT_CONDITION);
        for (int i = 0; i < currentCondition.length(); i++) {
            JSONObject object = currentCondition.getJSONObject(i);
            if (object.has(JSON_TAG_HUMIDITY)) {
                setHumidity(object.getInt(JSON_TAG_HUMIDITY));
            }
            if (object.has(JSON_TAG_PRESSURE)) {
                setPressure(object.getInt(JSON_TAG_PRESSURE));
            }
            if (object.has(JSON_TAG_TEMP_C)) {
                setTemp_C(object.getInt(JSON_TAG_TEMP_C));
            }
            if (object.has(JSON_TAG_TEMP_F)) {
                setTemp_F(object.getInt(JSON_TAG_TEMP_F));
            }
            if (object.has(JSON_TAG_VISIBILITY)) {
                setVisibility(object.getDouble(JSON_TAG_VISIBILITY));
            }
            if (object.has(JSON_TAG_LOCAL_OBS_DATE_TIME)) {
                setObservationTime(object.getString(JSON_TAG_LOCAL_OBS_DATE_TIME));
            }
            if (object.has(JSON_TAG_WEATHER_DESC)) {
                JSONArray weatherDescArray = object.getJSONArray(JSON_TAG_WEATHER_DESC);
                for (int j = 0; j < weatherDescArray.length(); j++) {
                    if (weatherDescArray.getJSONObject(j).has(JSON_TAG_VALUE)) {
                        setWeatherDesc(weatherDescArray.getJSONObject(j).getString(JSON_TAG_VALUE));
                        break;
                    }
                }
            }
            if (object.has(JSON_TAG_WEATHER_ICON_URL)) {
                JSONArray weatherDescArray = object.getJSONArray(JSON_TAG_WEATHER_ICON_URL);
                for (int j = 0; j < weatherDescArray.length(); j++) {
                    if (weatherDescArray.getJSONObject(j).has(JSON_TAG_VALUE)) {
                        setWeatherIconUrl(weatherDescArray.getJSONObject(j).getString(JSON_TAG_VALUE));
                        break;
                    }
                }
            }
        }
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getTemp_C() {
        return temp_C;
    }

    public void setTemp_C(int temp_C) {
        this.temp_C = temp_C;
    }

    public int getTemp_F() {
        return temp_F;
    }

    public void setTemp_F(int temp_F) {
        this.temp_F = temp_F;
    }

    public String getWeatherDesc() {
        return weatherDesc;
    }

    public void setWeatherDesc(String weatherDesc) {
        this.weatherDesc = weatherDesc;
    }

    public String getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public void setWeatherIconUrl(String weatherIconUrl) {
        this.weatherIconUrl = weatherIconUrl;
    }

    public double getVisibility() {
        return visibility;
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public String getObservationTime() {
        return observationTime;
    }

    public void setObservationTime(String observationTime) {
        this.observationTime = observationTime;
    }
}
