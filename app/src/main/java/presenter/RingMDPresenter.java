package presenter;

import android.os.Bundle;

/**
 * Created by trimao on 1/26/16.
 */
public interface RingMDPresenter {
    void onCreate(Bundle bundle);
}
