package presenter;

import android.os.Bundle;

import view.RingMDView;

/**
 * Created by trimao on 1/26/16.
 */
public class RingMDPresenterImpl implements RingMDPresenter {

    private final RingMDView ringMDViewModel;

    public RingMDPresenterImpl(RingMDView ringMDViewModel) {
        this.ringMDViewModel = ringMDViewModel;
    }

    @Override
    public void onCreate(Bundle bundle) {
        ringMDViewModel.initToolbar();
        ringMDViewModel.initViewPager();
    }

}
