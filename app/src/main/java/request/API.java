package request;

import android.content.ContentValues;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Map;

/**
 * Created by trimao on 1/27/16.
 */
public abstract class API {
    public static final String BASE_HOST = "http://api.worldweatheronline.com";
    private static final String KEY_REQUEST = "153b9d5a4e9b7b24435780b992253";
    private static final int NUMBER_OF_DAY = 1;
    private static final String FORMAT_RESPONSE = "json";
    private static final String FORMAT = "format";
    private static final String NUM_OF_DAYS = "num_of_days";
    private static final String KEY = "key";
    private static final String LOCAL_TIME = "localObsTime";
    private static final String EXTRA_KEY = "extra";

    public static String makeUrl(String path) {
        Uri.Builder builder = Uri.parse(BASE_HOST).buildUpon();
        builder.appendEncodedPath(path);

        return builder.build().toString();
    }

    public static String makeGetUrl(@NonNull String path, @NonNull ContentValues params) {
        Uri.Builder builder = Uri.parse(BASE_HOST).buildUpon();
        builder.appendEncodedPath(path);
        addParams(builder, params);

        return builder.build().toString();
    }

    private static void addParams(Uri.Builder builder, @Nullable ContentValues params) {
        if (params != null) {
            params.put(KEY, KEY_REQUEST);
            params.put(NUM_OF_DAYS, NUMBER_OF_DAY);
            params.put(FORMAT, FORMAT_RESPONSE);
            params.put(EXTRA_KEY, LOCAL_TIME);
            for (Map.Entry<String, Object> entry : params.valueSet()) {
                builder.appendQueryParameter(entry.getKey(), String.valueOf(entry.getValue()));
            }
        }
    }

    public static String makePostParams(ContentValues params) {
        return null;
    }

    public interface WEATHER {
        String LOCAL_WEATHER = "premium/v1/weather.ashx";
    }
}
