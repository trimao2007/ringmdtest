package request;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by trimao on 1/27/16.
 */
public abstract class BaseRequest<T> extends HttpRequest<T> {

    private static final String DATA = "data";
    private static final String RESULT_ERROR = "error";
    private static final String ERROR = "error";

    public BaseRequest(@NonNull String path, @Nullable ContentValues params, @Nullable Response.Listener<T> listener, @Nullable Response.ErrorListener errorListener) {
        this(API.makeGetUrl(path, params), Request.Method.GET, null, listener, errorListener);
    }

    public BaseRequest(int method, @NonNull String path, @Nullable ContentValues params, @Nullable Response.Listener<T> listener, @Nullable Response.ErrorListener errorListener) {
        this(API.makeUrl(path), method, API.makePostParams(params), listener, errorListener);
    }

    public BaseRequest(@NonNull String url, int method, @Nullable String params, @Nullable Response.Listener<T> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, params, listener, errorListener);
    }

    //parse response from the server
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String strJson;
        try {
            strJson = convertToString(response);
        } catch (UnsupportedEncodingException e) {
            return Response.error(new VolleyError("Server Error: unsupported encoding format exception."));
        } catch (OutOfMemoryError error) {
            try {
                strJson = convertToString(response);
            } catch (UnsupportedEncodingException | OutOfMemoryError ex) {
                return Response.error(new VolleyError("OutOfMemoryException during parsing data."));
            }
        }

        try {
            JSONObject json = new JSONObject(strJson);
            String result = json.toString();
            if (result != null && result.length() > 0) {
                return parseData(response, result);
            } else if (RESULT_ERROR.equalsIgnoreCase(result)) {
                return Response.error(new VolleyError(json.getString(ERROR)));
            } else {
                return Response.error(new VolleyError("Unexpected response"));
            }
        } catch (JSONException e) {
            return Response.error(new VolleyError("Unexpected JSONException"));
        }
    }

    // convert response to string for using next purpose
    private String convertToString(NetworkResponse response) throws UnsupportedEncodingException {
        return new String(response.data, HttpHeaderParser.parseCharset(response.headers));
    }

    protected abstract Response<T> parseData(NetworkResponse response, String string);

    // Attache params to header for each request
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        return headers;
    }
}
