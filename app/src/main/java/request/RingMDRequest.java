package request;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;

/**
 * Created by trimao on 1/27/16.
 */
public class RingMDRequest<T> extends BaseRequest<T> {

    public RingMDRequest(@NonNull String path, @Nullable ContentValues params, @Nullable Response.Listener<T> listener, @Nullable Response.ErrorListener errorListener) {
        super(path, params, listener, errorListener);
    }

    @Override
    protected Response<T> parseData(NetworkResponse response, String string) {
        T object = (T) string;
        return Response.success(object, null);
    }
}
