package utils;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by trimao on 1/30/16.
 */
public class DateTimeHelper {
    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
        Date latestUpdateTime = new Date();
        return dateFormat.format(latestUpdateTime);
    }

    public static String getHoursFromDateString(@NonNull String fullString) {
        if (fullString == null) return "";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.ENGLISH);
            DateFormat hourFormat = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
            Date date = dateFormat.parse(fullString);
            return hourFormat.format(date);
        } catch (Exception ex) {
            return "";
        }
    }
}
