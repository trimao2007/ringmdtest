package view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.example.trimao.ringmdtest.R;

import currentWeather.view.WeatherCityFragment;

/**
 * Created by trimao on 1/27/16.
 */
public class CityListAdapter extends FragmentPagerAdapter {

    private String [] listCity;
    public static final String CITY = "city";

    public CityListAdapter(FragmentManager fm, Context context) {
        super(fm);
        listCity = context.getResources().getStringArray(R.array.city_list);
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(CITY, listCity[position]);
        return WeatherCityFragment.newInstance(bundle);
    }

    @Override
    public int getCount() {
        return listCity.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listCity[position];
    }

    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }
}
