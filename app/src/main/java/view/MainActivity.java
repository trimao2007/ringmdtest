package view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.trimao.ringmdtest.R;

import presenter.RingMDPresenter;
import presenter.RingMDPresenterImpl;

public class MainActivity extends AppCompatActivity {

    private RingMDPresenter ringMDPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RingMDView ringMDViewModel = new RingMDViewImpl(this);
        ringMDPresenter = new RingMDPresenterImpl(ringMDViewModel);
        ringMDPresenter.onCreate(savedInstanceState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
