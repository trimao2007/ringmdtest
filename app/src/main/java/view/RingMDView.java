package view;

/**
 * Created by trimao on 1/26/16.
 */
public interface RingMDView {
    void initToolbar();
    void initViewPager();
}
