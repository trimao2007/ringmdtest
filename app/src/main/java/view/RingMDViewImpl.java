package view;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.trimao.ringmdtest.R;

/**
 * Created by trimao on 1/26/16.
 */
public class RingMDViewImpl implements RingMDView {

    private final AppCompatActivity activity;

    public RingMDViewImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initToolbar() {
    }

    @Override
    public void initViewPager() {
        CityListAdapter cityListAdapter = new CityListAdapter(activity.getSupportFragmentManager(), activity);
        ViewPager viewPager = (ViewPager) activity.findViewById(R.id.container);
        viewPager.setAdapter(cityListAdapter);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setCurrentItem(0);
    }

}
